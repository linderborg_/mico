# MICO

## Communication Channels

* Tasks @ https://issues.mico-project.eu
* Code @ https://bitbucket.org/mico-project/extractors.git
* Official documents @ Google Drive (never got access to it)
* MICO Umeå internal communications @ https://flp-umu.slack.com/messages/mico/
* Biweekly meetings with MICO Umeå
* Weekly e-mail reporting of
    * what has been done,
    * what will be done, and
    * how much time has been spent.

## Goals

* Create an internal version of speech to text for testing parameters.
* Find language models for Arabic and Italian.

## Result

* `kaldi_README_update`
    * Updated compile process for Kaldi (so that it only needs to be compiled once).
    * Clarified compile process for Kaldi.
* `kaldi_parameter_testing`
    * An internal version of speech to text for testing parameters. Based on a refactored version of the original code. The parameters that can be used as argument to the program are:
        * Kaldi specific
            * `default_chunk_size`
            * `default_segment_size`
            * `max_segment_size_sec`
        * MICO specific
            * `acoustic_scale`
            * `beam`
            * `lattice_beam`
            * `max_active`
* A list of organizations which may have Arabic and/or Italian language models.
* Testing material (audio/transciption) for Arabic and Italian.

# Experiences

* To set up MICO and Kaldi is noticeably time-consuming.
